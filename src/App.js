import logo from './logo.svg';
import './App.css';
import { Container, Row, Col, Button, Card, CardImg, CardText, CardBody, CardTitle,CardColumns } from 'reactstrap';
import { useEffect, useState } from 'react';
const faker = require('faker');

function App() {
  const [people, setPeople] = useState([]);


  function onClickGenerate() {
    setPeople([])
    for (let i = 0; i < 6; i++) {
      setPeople(people => [...people, {
        name: faker.name.findName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        phone : faker.phone.phoneNumberFormat(),
        city : faker.address.city(),
      }])
    }
  }

  function cards(p) {

    // console.log(p)
    return (
      <div>
        <Card style={{width:"100%"}}>
          <CardBody>
            <CardTitle tag="h5">{p.username}</CardTitle>
            <CardText>{p.name}</CardText>
            <CardText>{p.email}</CardText>
            <CardText>{p.city}</CardText>
            <CardText>{p.phone}</CardText>
          </CardBody>
        </Card>
      </div>
    )
  }

  return (
    <div className="App">
      
      <Button color="primary" onClick={onClickGenerate}>GENERATE</Button>
      <Container>
        
        <CardColumns className = "cards">
          {people != null ?
          (
            people.map((p) => {
              return cards(p)
            })
          
          ): null}
          </CardColumns>
        
      </Container>
    </div>
  );
}

export default App;
